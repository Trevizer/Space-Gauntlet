extends StaticBody2D

#Marker2D indica donde van a hacer spawn los enemigos

#precargamos la escena del enemigo
var enemyLoad = preload("res://badguy.tscn")
#var player = null
var on_screen = 0
var grupos = null
var random 
var random_group
var golpes: int

#máximo de enemigos que puede spawnear a la vez
var maxenemies = 5
var enemiesNow = 0 

var creandoEnemy = 0 #variable de control si está creando enemigos
var tiempoSpawn = 0.5 #tiempo entre cada spawn de enemigo

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite2D.animation = "first"
	golpes = 0
	random_group = _randomGroup()
	random_group = str(random_group)  #convertirmos el grupo en un string
	add_to_group("enemyhouse")
	add_to_group(random_group)
	grupos = get_groups()
	Global.enemydead.connect(_on_enemydead)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	
	if (enemiesNow < maxenemies) and (on_screen == 1):
		if creandoEnemy == 0:
			createEnemy()
		

func createEnemy():
	creandoEnemy = 1
	await get_tree().create_timer(tiempoSpawn).timeout
	var enemy = enemyLoad.instantiate()
	enemy.global_position = $SpawnPosition.global_position
	enemy.add_to_group(random_group)
	#get_parent().add_child(enemy)
	
	get_parent().call_deferred("add_child", enemy)
	enemiesNow = enemiesNow + 1
	creandoEnemy = 0
	
###### ESTAS 2 FUNCIONES SON PARA QUE LAS CASAS NO HAGAN SPAWN
###### SI EL PLAYER NO ESTÁ A LA VISTA		
func _on_VisibleOnScreenNotifier2D_screen_entered():
	on_screen = 1
	
func _on_VisibleOnScreenNotifier2D_screen_exited():
	on_screen = 0
##########################################################
##########################################################


##### RECOGEMOS LA SEÑAL DE GLOBAL PARA IDENTIFICAR LA CASA DEL ENEMIGO
func _on_enemydead(body):
	if body.is_in_group(random_group):
		enemiesNow = enemiesNow - 1
	pass
	

#LA SIGUIENTE FUNCIÓN ASIGNA UN GRUPO ALEATORIO A CADA CADA GENERADA
func _randomGroup():
	seed("akunamatata".hash())
	random = RandomNumberGenerator.new()
	random.randomize()
	return (random.randi_range(200, 900))
	
func dosGolpes():
	golpes = golpes + 1
	$AnimatedSprite2D.animation = "second"


