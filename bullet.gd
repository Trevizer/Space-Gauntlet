extends Area2D

#signal enemydead #señal para identificar que hemos matado a un enemigo


@export var speed = 200
#var direction = Vector2(1.0,0.0)
var velocity = Vector2(1,1)
var direction = Vector2(1.0,0.0)

func _physics_process(delta):
	position = position + speed * direction * delta




func _on_body_entered(body):
	if (body.is_in_group("scenary")) or (body.is_in_group("door")):
		queue_free()
	elif body.is_in_group("enemy"):
		Global.score = Global.score + 100
		Global.enemydead.emit(body) #emitimos señal de que matamos a un enemigo para comprobar su casa
		body.queue_free()
		self.queue_free()
	elif body.is_in_group("enemyhouse"):
		Global.score = Global.score + 500
		Global.sound_enemyhouse.emit("first") #emite el sonido del primer golpe
		body.dosGolpes()
		if body.golpes == 2:
			Global.sound_enemyhouse.emit("second")
			body.queue_free()			
		self.queue_free()
	
