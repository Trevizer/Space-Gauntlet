extends Node2D
#MAP 5


var next_scene
var mapa


func _ready():
	Global.currentMap = "cinco"

func _process(delta):
	pass


func _on_exit_body_entered(body):
	if (Global.currentMap == "cinco") and (body.is_in_group("player")):
		Global.get_exit_sound.emit()
		next_scene = "ending" #pasamos a la escena final
		Global.changeMap.emit(next_scene)
	
